from django.contrib import admin
from django.urls import path
from myprofile import views

urlpatterns = [
    path('', views.index, name='index'),
    path('showcase',views.showcase,name='showcase'),
    path('contact',views.contact,name='contact'),
    path('schedule', views.show, name="show"),
    path('add', views.addSched, name="add"),
    path('delete/<int:id>', views.bigdelete, name="erase")
]