from django.shortcuts import render,get_object_or_404
from myprofile import forms
from .forms import Forms
from django.urls import reverse
from django.http import HttpResponseRedirect
from .models import Schedule


def index(request):
    return render(request, 'profile.html')

def home(request):
    return render(request, 'home.html')

def showcase(request):
    return render(request, 'showcase.html')

def contact(request):
    return render(request,'contact.html')


def addSched(request):
  formSched = Forms(request.POST)
  if(request.method == 'POST'):
    if(formSched.is_valid()):
      clean = formSched.cleaned_data
      schedule = Schedule()
      schedule.event = clean['event']
      schedule.datetime = clean['datetime']
      schedule.location = clean['location']
      schedule.personel = clean['personel']
      schedule.time = clean['time']
      schedule.save()
      return  HttpResponseRedirect('schedule')

# menampilkan html dengan input forms dan mendisplay models
def show(request):
  response = {
    'showSched' : Schedule.objects.all(),
    'schedules' : Forms
  }
  return render(request,"forms.html",response)

def bigdelete(request, id):
    deletion = get_object_or_404(Schedule, pk=id)
    deletion.delete()
    return HttpResponseRedirect(reverse('show'))



