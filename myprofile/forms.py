from django import forms
from myprofile import models
from django.utils import timezone

class Forms(forms.Form):
    event_attrs = { 
        "type":"text",
        "placeholder":"Event Name",
        "name":"event",
        "value":"",
    }
    calendar_attrs = {
        "type":"date",
        "name":"datetime",
        "value":"",
    }
    locations_attrs = {
        "type":"text",
        "placeholder":"Location",
        "name":"location",
        "value":"",
    }
    personel_attrs = {
        "type":"text",
        "placeholder":"name",
        "name":"personel",
        "value":"",
    }
    time_attrs = {
        "type":"time",
        "name":"time",
        "value":"",
    }

    datetime = forms.DateField(initial=timezone.now, required = True, widget=forms.DateInput(attrs=calendar_attrs))
    time = forms.TimeField(initial=timezone.now, required=True,widget=forms.TimeInput(attrs=time_attrs))
    event = forms.CharField(max_length=200, required = True,widget=forms.TextInput(attrs=event_attrs))
    location = forms.CharField(max_length=200, required = True,widget=forms.TextInput(attrs=locations_attrs))
    personel = forms.CharField(max_length=200, required = True,widget=forms.TextInput(attrs=personel_attrs))
