from django.db import models


# Create your models here.
class Schedule(models.Model):
  id = models.AutoField(primary_key=True)
  datetime = models.DateField()
  time = models.TimeField()
  event = models.CharField(max_length=200)
  location = models.CharField(max_length =200)
  personel = models.CharField(max_length =200)
